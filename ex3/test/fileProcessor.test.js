const assert = require('assert');
const proxyquire = require('proxyquire');
// let fileProcessor = require('../src/fileProcessor');

describe('fileProcessor', function () {

    describe('#handleNewFile()', function () {

        let getTermFrequencyFake, getNewDocFrequencyFake, calculateTfIdfFake;
        beforeEach(function () {

            fileProcessor = proxyquire('../src/fileProcessor', {
                './tfIdfHelper': {
                    getTermFrequency: () => getTermFrequencyFake,
                    getNewDocFrequency: () => getNewDocFrequencyFake,
                    calculateTfIdf: () => calculateTfIdfFake
                }
            });
        });

        it('should update programInfo correctly for 1 file and 3 terms', async function () {

            let programInfo = {
                'fileInfo': {},
                'numDocuments': 0
            };
            const terms = ['word2', 'word3', 'word4'];
            let fileStream;
            const termFrequency = {
                'word2': 0,
                'word3': 23,
                'word4': 1
            };
            const docFrequency = {
                'word2': 1,
                'word3': 4,
                'word4': 2
            };
            const tfIdf = 2.4;

            const expectedResult = {
                'fileInfo': {
                    'file1': {
                        'termFrequency': termFrequency,
                        'tfIdf': tfIdf
                    }
                },
                'docFrequency': docFrequency,
                'numDocuments': 1
            };

            // stubbing some functions
            getTermFrequencyFake = Promise.resolve(termFrequency);
            getNewDocFrequencyFake = docFrequency;
            calculateTfIdfFake = tfIdf;

            // SUT
            const result = await fileProcessor.handleNewFile('file1', fileStream, terms, programInfo);

            // Assertions
            assert.deepEqual(expectedResult, result);
        });

    });
});

