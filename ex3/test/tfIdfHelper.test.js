const assert = require('assert');
const stream = require('stream');
const tfIdfHelper = require('../src/tfIdfHelper');

/**
 * Generator function to create a stream
 */
function* gen() {
    yield 'word1 word2.\n';
    yield 'word3 ';
    yield 'word3 word4';
}

describe('tfIdfHelper', function () {

    const terms = ['word2', 'word3', 'word4'];

    describe('#getTermFrequency()', function () {

        it('should return correct term frequency for all terms', async function () {
            const readableStream = stream.Readable.from(gen(), { encoding: 'utf8' });

            let result = await tfIdfHelper.getTermFrequency(readableStream, terms);

            const expectedResult = {
                'word2': 0.6931471805599453,
                'word3': 1.0986122886681096,
                'word4': 0.6931471805599453
            }

            assert.deepEqual(expectedResult, result);
        });
    });

    describe('#calculateTfIdf()', function () {

        it('should return the correct calculateTfIdf', function () {
            let programInfo = {
                'fileInfo': {
                    'file1': {
                        'termFrequency': {
                            'word2': 0.6931471805599453,
                            'word3': 1.0986122886681096,
                            'word4': 0.6931471805599453
                        }
                    }
                },
                'docFrequency': {
                    'word2': 1,
                    'word3': 1,
                    'word4': 2
                },
                'numDocuments': 2
            }

            const result = tfIdfHelper.calculateTfIdf('file1', terms, programInfo);

            const expectedResult = 1.1404948252147773;
            assert.equal(expectedResult, result);

        });
    });
});