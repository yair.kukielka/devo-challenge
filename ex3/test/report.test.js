const assert = require('assert');
const reporter = require('../src/reporter');

describe('reporter', function () {

    describe('#getDataToReport()', function () {
        it('should return sorted data', function () {
            const fileInfo = {};
            fileInfo['a'] = { 'tfIdf': 1 };
            fileInfo['b'] = { 'tfIdf': 19 };
            fileInfo['c'] = { 'tfIdf': 0 };

            const data = [];
            data.push(['b', 19]);
            data.push(['a', 1]);
            data.push(['c', 0]);

            const result = reporter.getDataToReport(fileInfo, 3);
            assert.deepEqual(data, result);
        });

        it('should return only first 2 items', function () {
            const fileInfo = {};
            fileInfo['a'] = { 'tfIdf': 1 };
            fileInfo['b'] = { 'tfIdf': 19 };
            fileInfo['c'] = { 'tfIdf': 0 };

            const data = [];
            data.push(['b', 19]);
            data.push(['a', 1]);

            const result = reporter.getDataToReport(fileInfo, 2);
            assert.deepEqual(data, result);
        });

        it('should return only available items when requested more', function () {
            const fileInfo = {};
            fileInfo['a'] = { 'tfIdf': 1 };
            fileInfo['b'] = { 'tfIdf': 19 };
            fileInfo['c'] = { 'tfIdf': 0 };

            const data = [];
            data.push(['b', 19]);
            data.push(['a', 1]);
            data.push(['c', 0]);

            const numDocsToReport = 33;
            const result = reporter.getDataToReport(fileInfo, numDocsToReport);
            assert.deepEqual(data, result);
        });
    });
});

