const assert = require('assert');
const sinon = require('sinon');
const proxyquire = require('proxyquire');


describe('tfIdf', function () {

    describe('#run()', function () {
        let tfIdf;
        let getArgsStub, watchStub;
        beforeEach(function () {
            getArgsStub = sinon.stub();
            watchStub = sinon.stub();

            tfIdf = proxyquire('../src/tfIdf', {
                './IOUtils': {
                    getArgs: getArgsStub
                },
                './watcher': {
                    watch: watchStub
                }
            });

        });

        it('should call tfIdf.run and watcher.watch once each', async function () {

            let argv = {
                t: 'two words',
                d: 'dir',
                n: 3,
                p: 2000
            };
            getArgsStub.returns(argv);
            watchStub.resolves(Promise.resolve());

            tfIdf.run();

            assert.equal(getArgsStub.calledOnce, true);
            assert.equal(watchStub.calledOnce, true);
        });
    });
});

