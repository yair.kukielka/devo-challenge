
/**
 * Splits a stream chunk into an array or words
 * @param {string} chunk 
 */
const splitChunkInWords = (chunk) => {
    const wordsArray = chunk
        .replace(/[.,?!;()"'-]/g, ' ')
        .toLowerCase()
        .split(/\s+/);
    return wordsArray;
}
/**
 * Adds terms occurences in wordsArray to termCount
 * @param {string []} terms 
 * @param {string []} wordsArray 
 * @param {Object} termCount Object that holds the mapping between term and its occurences
 */
const addTermCount = (terms, wordsArray, termCount) => {
    wordsArray.forEach(word => {
        if (word) {
            terms.forEach(term => {
                if (word === term) {
                    if (!termCount[term]) {
                        termCount[term] = 1;
                    } else {
                        termCount[term] += 1;
                    }
                }
            });
        }
    });
}

/**
 * Returns the term count of a stream in an object with this structure
 * { 'word1': 23, 'word2': 2, 'word3': 0}
 * 
 * @param {Object (stream)} readStream
 * @param {string []} terms 
 * @returns Object with the term count
 */
const getTermCount = async (readStream, terms) => {
    let wordsArray;
    let remaining = '';
    let termCount = {};
    for await (const chunk of readStream) {
        remaining += chunk;
        wordsArray = splitChunkInWords(remaining);

        if (wordsArray.length && wordsArray[wordsArray.length - 1] != '') {
            // the last word of the chunk is trunked. Save for concatenating to next chunk 
            remaining = wordsArray.pop();
        } else {
            remaining = '';
        }
        addTermCount(terms, wordsArray, termCount);
    }
    if (remaining) {
        // add last word that was cut off
        addTermCount(terms, [remaining], termCount);
    }
    return termCount;
}

/**
 * Increases the total document frequency if the term was present in the document.
 * It does this checking for each term if it has termFrequency != 0
 * @param {string []} terms 
 * @param {Object} termFrequency 
 * @param {Object} docFrequency
 * @returns the updated document frequency 
 */
const getNewDocFrequency = (terms, termFrequency, docFrequency) => {
    for (let term of terms) {
        if (docFrequency[term] == null) {
            docFrequency[term] = 0; // initialize value
        }
        if (termFrequency[term]) {
            docFrequency[term] += 1;
        }
    }
    return docFrequency;
}


/**
 * Returns the inverse document frequency of the term in all the documents.
 * It uses an adjustingValue of 0.1 to avoid div by 0 and 
 * returning negative numbers (log normalization)
 * @param {string} term 
 * @param {Object} programInfo 
 * @returns the idf
 */
const calculateInverseDocumentFrequency = (term, programInfo) => {
    const adjustingValue = 0.1;
    const numDocsWhereTermIsPresent = programInfo.docFrequency[term];
    if (programInfo.numDocuments <= 0.1 + numDocsWhereTermIsPresent) {
        return adjustingValue;
    }
    return Math.log(programInfo.numDocuments / (adjustingValue + numDocsWhereTermIsPresent));
}

/**
 * Returns the tfIdf, which is calculated adding the tfIdfs of all terms for a document
 * @param {string} fileName 
 * @param {string []} terms 
 * @param {Object} programInfo
 * @returns the Tf-Idf of all terms fir this document
 */
const calculateTfIdf = (fileName, terms, programInfo) => {
    const totalTfIdf = terms.reduce((total, term) => {
        let termFrequency = 0;
        if (programInfo.fileInfo[fileName] &&
            programInfo.fileInfo[fileName].termFrequency &&
            programInfo.fileInfo[fileName].termFrequency[term]) {
            termFrequency = programInfo.fileInfo[fileName].termFrequency[term];
        }
        const inverseDocumentFrequency = calculateInverseDocumentFrequency(term, programInfo);
        const termTfIdf = termFrequency * inverseDocumentFrequency;
        return total += termTfIdf;
    }, 0);
    return totalTfIdf;
}

/**
 * Returns the term frequency. This is calculated by the Math.log(1 + count), 
 * being 'count' the number of occurences of the term in the file. The constant 1 
 * is used to avoid returning negative numbers (log normalization)
 * @param {Object (stream)} readStream 
 * @param {string []} terms 
 */
const getTermFrequency = async (readStream, terms) => {
    const termCount = await getTermCount(readStream, terms);
    let termFrequency = {};
    for (let [term, count] of Object.entries(termCount)) {
        termFrequency[term] = count ? Math.log(1 + count) : 0;
    }
    return termFrequency;
}

module.exports = {
    getNewDocFrequency,
    calculateTfIdf,
    getTermFrequency,
    getTermCount
}