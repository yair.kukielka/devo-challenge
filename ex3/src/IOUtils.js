const fs = require('fs');

const getFileStream = (fileName) => fs.createReadStream(fileName, { encoding: 'utf-8', highWaterMark: 8192 });

/**
 * Reads the args from command line using 'minimist' and returns them.
 * If any argument is not provided the program prints an error message and exits.
 * @param {Object} argv 
 * @returns {Object} object with args
 */
const getArgs = () => {
    let argv = require('minimist')(process.argv.slice(2));
    if (!argv.d || !argv.n || !argv.p || !argv.t) {
        console.log('Missing a parameter');
        console.log('Required args: d, n, p, t');
        console.log('Example: node index -d ./textFiles -n 5 -p 1000 -t "password try again"');
        process.exitCode = 1;
    } else {
        return argv;
    }
}

module.exports = {
    getFileStream,
    getArgs
}