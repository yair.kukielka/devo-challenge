/**
 * Gathers the information of the tf-idfs for all files, sorts them and prints them.
 * @param {Object} fileInfo Object that maps a file with it's tfIdf data
 * @param {Number} topResultsNum number of files to report about
 * @returns {[string,Number][]} array where each element is an array of a string (filename) and a Number (tfIdf value)
 */
const getDataToReport = (fileInfo, topResultsNum) => {
    let reportData = [];
    for (let [fileName, value] of Object.entries(fileInfo)) {
        if (fileInfo != null) {
            reportData.push([fileName, value.tfIdf]);
        }
    }
    reportData.sort((a, b) => b[1] - a[1]); // sort by tfIdf value
    return reportData.slice(0, Math.min(reportData.length, topResultsNum));
}

module.exports = {
    getDataToReport
}