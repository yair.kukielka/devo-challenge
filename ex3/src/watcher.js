const fs = require('fs');
const reporter = require('./reporter');
const fileProcessor = require('./fileProcessor');
const IOUtils = require('./IOUtils');

const wait = ms => new Promise(resolve => setTimeout(resolve, ms));

const watch = async ({ filesDir, terms, topResultsNum, periodToReport, print }) => {
    console.log('listening to ' + filesDir + ' for changes...');

    // Initialization. This obj keeps all necessary info in memory
    let programInfo = {
        fileInfo: {}, //object to map fileNames to objects like {'termFrequency':{'house':0.34}, 'tfIdf':0.2}}
        docFrequency: {}, // object mapping a term to its total document frequency
        numDocuments: 0 // number of documents read
    }

    fs.watch(filesDir, async (event, fileName) => {

        if (fileName && event === 'change') {
            fileName = filesDir + '/' + fileName;
            try {
                const fileStream = IOUtils.getFileStream(fileName);
                programInfo = await fileProcessor.handleNewFile(fileName, fileStream, terms, programInfo);
            } catch (err) {
                console.log(`Error while handling new file ${fileName}`, err);
            }
        }
    });

    while (true) {
        // report top N files every period
        await wait(periodToReport);
        print(reporter.getDataToReport(programInfo.fileInfo, topResultsNum, print));
    }
}

exports.watch = watch;

