const tfIdfHelper = require('./tfIdfHelper');

/**
 * Handles every new detected file: 
 * 1 - it processes the new file (calculates term frequencies and total document frequency)
 * 2 - it recalculates tf-idf for all files
 * This method only recalculates the necessary parts (Idf values) to get the 
 * new tf-idf values, keeping untouched values like term frequencies of 
 * previously processed files.
 * @param {string} fileName
 * @param {Object (stream)} fileStream
 * @param {string []} terms 
 * @param {Object} programInfo program information object
 * @returns {Object} programInfo object, with updated values 
 */
const handleNewFile = async (fileName, fileStream, terms, programInfo) => {
    try {
        const termFrequency = await tfIdfHelper.getTermFrequency(fileStream, terms);
        const docFrequency = tfIdfHelper.getNewDocFrequency(terms, termFrequency, programInfo.docFrequency);
        programInfo.docFrequency = docFrequency;
        programInfo.fileInfo[fileName] = {};
        programInfo.fileInfo[fileName].termFrequency = termFrequency;
        programInfo.numDocuments += 1;
    } catch (err) {
        console.log(`Error while processing file ${fileName}`, err);
    }

    for (const document in programInfo.fileInfo) {
        // recalculate all tfIdf's of previous files because total document number changed
        programInfo.fileInfo[document].tfIdf = tfIdfHelper.calculateTfIdf(document, terms, programInfo);
    }
    return programInfo;
}

module.exports = {
    handleNewFile
}