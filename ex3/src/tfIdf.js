let watcher = require('./watcher');
const IOUtils = require('./IOUtils');

/**
 * This is a way to output the result of the program. It is used to decouple the 
 * way the information is output and also so it can be replaced during 
 * testing by another function given that testing console output is hard.
 * 
 * @param {[string,Number][]} data array where each element is an array 
 * of a string (filename) and a Number (tfIdf value)
 */
const print = (data) => {
    let printBlankLine = false;
    for (let elem of data) {
        console.log(`${elem[0]}  ${elem[1]}`);
        printBlankLine = true;
    }
    if (printBlankLine)
        console.log('')
}

const run = async () => {
    try {
        const args = IOUtils.getArgs();
        const terms = args.t.split(" ").map(term => term.toLowerCase()); // for comparisons
        const filesDir = args.d;
        const topResultsNum = args.n;
        const periodToReport = args.p;

        await watcher.watch({ filesDir, terms, topResultsNum, periodToReport, print });

    } catch (err) {
        console.log(`Error in the program`, err);
    }
};

exports.run = run;
