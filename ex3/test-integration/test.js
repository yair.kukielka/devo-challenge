const assert = require('assert').strict;
const reporter = require('../src/reporter');
const IOUtils = require('../src/IOUtils');
const fileProcessor = require('../src/fileProcessor');

const assertCorrectData = (data) => {
    assert.deepEqual(
        [
            ['./test-integration/testFiles/short1.txt', 0.48370374956416673],
            ['./test-integration/testFiles/short2.txt', 0.06931471805599453]
        ]
        , data);
}

(async () => {
    try {
        // Initialization. This obj keeps all necessary info in memory
        let programInfo = {
            'fileInfo': {}, //object to map fileNames to objects like {'termFrequency':{'house':0.34}, 'tfIdf':0.2}}
            'docFrequency': {}, // object mapping a term to its total document frequency
            'numDocuments': 0 // number of documents read
        }

        let fileNames = ['./test-integration/testFiles/short1.txt', './test-integration/testFiles/short2.txt'];
        let topResultsNum = 5;
        let terms = ['term', 'ipsum', 'Lorem'];

        for (fileName of fileNames) {
            const fileStream = IOUtils.getFileStream(fileName);
            programInfo = await fileProcessor.handleNewFile(fileName, fileStream, terms, programInfo);
        }
        let data = reporter.getDataToReport(programInfo.fileInfo, topResultsNum);
        assertCorrectData(data);
    } catch (e) {
        console.log(`Error testing `, e);
    }
})();

