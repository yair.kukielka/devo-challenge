const assert = require('assert');

// Write an efficient algorithm to find K-complementary pairs in a given array of integers.
// Given Array A, pair (i, j) is K-complementary if K = A[i] + A[j];

/**
 * Returns the K-complementary pairs of it's arguments
 * @param {number[]} array of numbers
 * @param {number} k the target number
 * @returns Array that contains arrays of 2 numbers that are k-complementary
 */
const complementary = (array, k) => {
    let mapK = {}; // map to temporarily keep k-complementary pairs as key-value
    const resultPairs = []; // array to save k-complementary pairs found

    for (let i = 0; i < array.length; i++) {
        const element = array[i];

        if (mapK[element] != null) {
            resultPairs.push([element, mapK[element]]);
        } else {
            // add the pair to the map as key-value
            mapK[k - element] = element;
        }
    }
    return resultPairs;
}

const printPairs = (a, k, pairs) => {
    console.log('array ' + a);
    console.log('number ' + k);
    console.log('pairs: ');
    pairs.forEach(pair => {
        console.log(`    ${pair[0]}, ${pair[1]}`);
    });
}

const test = (expected, actualResult, ) => {
    console.log(`Expected result: ${expected}`);
    console.log(`Actual result:   ${actualResult}`);
    assert.deepEqual(actualResult, expected);
    console.log();
}

let a = [0, 1.4, 1.6, 3, 4, 5, 6];
let k = 3;
let pairs = complementary(a, k);
printPairs(a, k, pairs);
test([[1.6, 1.4], [3, 0]], complementary(a, k));

a = [8, 0, -1, 6, 14, -22];
k = 16;
pairs = complementary(a, k);
printPairs(a, k, pairs);
test([], complementary(a, k));

a = [8, 0, -1, 6, 14, -22, 15];
k = 14;
pairs = complementary(a, k);
printPairs(a, k, pairs);
test([[6, 8], [14, 0], [15, -1]], complementary(a, k));


a = [8, 0, -1, 6, 14, -22, 15];
k = -1;
pairs = complementary(a, k);
printPairs(a, k, pairs);
test([[-1, 0]], complementary(a, k));


a = [];
k = 0;
pairs = complementary(a, k);
printPairs(a, k, pairs);
test([], complementary(a, k));