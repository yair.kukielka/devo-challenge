# README

This is a code challenge with 3 exercises. All of them have been written en Nodejs, so you will need to have node and npm installed before running them.

## Excercise 1 
Write an efficient algorithm to check if a string is a palindrome. A string is a palindrome if the string matches the reverse of string.

The code is in the ex1 folder. To run the code and the tests just do:
```
cd ex1
node palindrome.js
```

* Time complexity O(n)
* Space complexity O(n)
  
Being n the length of the string

## Excercise 2 
Write an efficient algorithm to find K-complementary pairs in a given array of integers.
Given Array A, pair (i, j) is K- complementary if K = A[i] + A[j];


The code is in the ex2 folder. To run the code and the tests just do:
```
cd ex2
node complementary.js
```

* Time complexity O(n)
* Space complexity O(n)
  
Being n the length of the array


## Excercise 3 
Tf/idf (term frequency / inverse document frequency) is an statistic that reflects the importance of a term T in a document D (or the relevance of a document for a searched term) relative to a document set S.

See https://en.wikipedia.org/wiki/Tf%E2%80%93idf 
Tf/idf can be extended to a set of terms TT adding the tf/idf for each term. 

Assume that we have a directory D containing a document set S, with one file per document. Documents will be added to that directory by external agents,but they will never be removed or overwritten. We are given a set of terms TT, and asked to compute the tf/idf of TT for each document in D, and report the N top documents sorted by relevance. 
The program must run as a daemon/service that is watching for new documents, and dynamically updates the computed tf/idf for each document and the inferred ranking.

The program will run with the parameters: 

* The directory D where the documents will be written. 
* The terms TT to be analyzed. 
* The count N of top results to show. 
* The period P to report the top N. For example:
* 
``` 
./tdIdf -d dir -n 5 -p 300 -t "password try again" ... 
Result examples:
doc1.txt 0.78 
doc73.txt 0.76
...
```

The code is in the ex3 folder. To install the necessary packages run:
```
cd ex3
npm install
```
And then start the program with either of these commands:
```
npm start -- -d ./textFiles -n 5 -p 1000 -t "term ipsum lorem"
```
or 
```
node index -d ./textFiles -n 5 -p 1000 -t "term ipsum lorem"
```

To run the tests run:

```
npm test
```

There is also an integration test that you can run with:

```
npm run test-integration
```

* Time complexity O(n1 + (w1 * t) + n2 + (w2 * t) + n3 + (w3 * t)...)
* Space complexity O(t * d)
  
Being:
* n1 the number of characters in document 1, n2, characters in document 2...
* w1 the number of words in document 1, w2 the number of words in document 2...
 (I have split words where there is one of the next characters: /[.,?!;()"'-]/ and one or more spaces)
* t is the number of terms passed in the t argument (after splitting by a space)
* d is the number of documents


