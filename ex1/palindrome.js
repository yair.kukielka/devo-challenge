const assert = require('assert');
// Write an efficient algorithm to check if a string is a palindrome. 
// A string is a palindrome if the string matches the reverse of string.

/**
 * Tells if the string is a palindrome
 * @param {string} s A string
 * @returns True if s is a palindrome. False otherwise
 */
const palindrome = (s) => {
    let i = 0;
    let j = s.length - 1;
    while (i < j) {
        if (s[i++] != s[j--]) {
            return false;
        }
    }
    return true;
}

const test = (expected, actualResult, ) => {
    console.log(`Expected ${expected}, actual result ${actualResult}`);
    assert.equal(actualResult, expected);
}

test(palindrome(''), true);
test(palindrome('a'), true);
test(palindrome('aa'), true);
test(palindrome('ab'), false);
test(palindrome('aaa'), true);
test(palindrome('aeaea'), true);
test(palindrome('aeae1a'), false);